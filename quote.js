var prev=0;


$("#quotebutton").on("click", update);
  $("dl").css("fontSize", "16px");
 $("dt").css("fontSize", "19px");

function update(){
  change();
 createButton(); 
}
function createButton()
{
 $("#twtbox").empty();
  var link = document.createElement('a');
  link.setAttribute('href', 'https://twitter.com/share');
  link.setAttribute('class', 'twitter-share-button');
  link.setAttribute('style', 'margin-top:5px;');
  link.setAttribute('id', 'twitterbutton');
  link.setAttribute("data-text", tweettext);
  link.setAttribute("data-size", "large");
  tweetdiv = document.getElementById('twtbox');
  tweetdiv.appendChild(link);
  twttr.widgets.load();
}



function change() {
  var quotes = ["Don't cry because it's over, smile because it happened@Dr. Seuss", "In the end, it's not the years in your life that count. It's the life in your years.@Abraham Lincoln", "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.@Albert Einstein", "Be who you are and say what you feel, because those who mind don't matter, and those who matter don't mind.@Bernard M. Baruch", "A room without books is like a body without a soul.@Marcus Tullius Cicero", "So many books, so little time.” @Frank Zappa", "Be the change that you wish to see in the world.@Mahatma Gandhi", "If you tell the truth, you don't have to remember anything.@Mark Twain"];
var num=Math.floor(Math.random() * quotes.length);
  while(num==prev){
    num=Math.floor(Math.random() * quotes.length);
  }
  prev=num;
  var quotedisplay = quotes[num].split("@");
  
  tweettext = quotedisplay[0] + " --"+ quotedisplay[1] ;
  document.getElementById('quote').innerHTML =quotedisplay[0];
  document.getElementById('author').innerHTML = "--" + quotedisplay[1];

}

$(document).ready(function() {

  var tweettext = "";

  change();
  createButton();

  $("dt").css("fontSize", "19px");
  $("dl").css("fontSize", "16px");

  
}